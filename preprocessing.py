import numpy as np
import pickle, time,os,sys

# IMPORT MY SCRIPTS/FUNCTIONS
from get_data import get_data
from build_compile_model_2D_cnn_cal_vel import build_compile_model_2D_cnn_cal_vel
from build_compile_model_2D_cnn_cal_vel_pos import build_compile_model_2D_cnn_cal_vel_pos
from build_compile_model_1D_cnn import build_compile_model_1D_cnn
from custom_split import custom_split
from custom_upsampling import custom_upsampling
from take_labels import take_labels, fix_training_set
from custom_accuracy import custom_accuracy

problem_type = 'multiclass'  # options for problem_type: binary, multiclass, multilabel
dataset_type = 'tristan_classes'    # options for dataset_type: wenke, tristan_classes, tristan_markers
batch = '13_mice'                   # options for batch: 1_2, 3, 13_mice
laps = 'yes'
normed ='without'
chance=False

num_iters = 10
epochs = 50

features = 3# option for features: 1 (calcium), 2(calcium-velocity), 3(calcium-velocity-position)
num_classes = 6 # Which labels we use
category = 'semi_balanced' # train set representation of each class (semi_balanced, imbalanced, min_categ)
space = 'no'

interp_timesteps = 100
step_laps = 2 # merging laps
threshold_in_laps = False#['max',0.0]
threshold_out_laps = False#['max',0.0]
z_depth = 'no'
balance= 'balanced' # balanced subcategories per merged classes
loss_func = 'categorical_crossentropy'
# loss_func = 'categorical_focal_loss'
learning_rate=0.001
upsample = 'no'
sigma = 0.1

test_set = 'laps' # cells or laps: test on whole cell-laps, or on laps individually
level = 0.5 # level above which a cell is correctly classified, if 'cells' used

# INITIALIZATIONS
train_accuracy = []
test_accuracy = []
time_elapsed = []

if num_classes==3:
    indexing = [['BC','AAC'],['BISTR','SOM'], ['CCK']]
    # class names for saving
    class_names = ['BC-AAC','BISTR-SOM','CCK']
    # If LFP
    LFP = [0.20564221386694392, 0.1213351266197088, 0.15877150163471782]
    LFP = [i/max(LFP) for i in LFP]
elif num_classes==4:
    indexing = [['BC','AAC'],['BISTR','SOM'], ['CCK'], ['NPY']]
    # class names for saving
    class_names = ['BC-AAC','BISTR-SOM','CCK', 'NPY']
    # If LFP
    LFP = [0.20564221386694392, 0.1213351266197088, 0.15877150163471782, 0.18573870468279013]
    LFP = [i/max(LFP) for i in LFP]
elif num_classes==5:
    indexing = [['BC'],['AAC'],['BISTR'],['SOM'], ['CCK']]
    # class names for saving
    class_names = ['BC','AAC','BISTR','SOM','CCK']
    # If LFP
    LFP = [0.2441068310145397, 0.16717759671934812, 0.13040958762968752, 0.11226066560973007, 0.15877150163471782]
    LFP = [i/max(LFP) for i in LFP]
elif num_classes==6:
    indexing = [['BC'],['AAC'],['BISTR'],['SOM'], ['CCK'], ['NPY']]
    # class names for saving
    class_names = ['BC','AAC','BISTR','SOM','CCK','NPY']
    # If LFP
    LFP = [0.2441068310145397, 0.16717759671934812, 0.13040958762968752, 0.11226066560973007, 0.15877150163471782, 0.18573870468279013]
    LFP = [i/max(LFP) for i in LFP]

if len(indexing)!=len(class_names):
    sys.exit('Error in indexing or name formats.')
# balanced test set
if test_set=='cells':
    test_stratified = 'no'
    if test_stratified=='no':
        if num_classes==3:
            test_size =[[10,10],[10,10],[20]]
        elif num_classes==4:
            test_size =[[10,10],[10,10],[20], [20]]
        elif num_classes==5:
            test_size =[[10],[10],[10],[10],[10]]
        elif num_classes==6:
            test_size =[[10],[10],[10],[10],[10],[10]] 
    else:
        test_size =[[75,75],[75,75],[75],[75]]
elif test_set=='laps':
    test_stratified = 'no'
    if test_stratified=='no':
        if num_classes==3:
            test_size =[[75,75],[75,75],[150]]
        elif num_classes==4:
            test_size =[[75,75],[75,75],[150],[150]]
        elif num_classes==5:
            test_size =[[75],[75],[75],[75],[75]]
        elif num_classes==6:
            test_size =[[75],[75],[75],[75],[75],[75]] 
    else:
        test_size =[[75,75],[75,75],[75],[75]]

if len(test_size)!=len(class_names):
    sys.exit('Error in test-size.')
#------------------------------------------------------------------------------------------------ 

# GET THE DATA 
if category=='semi_balanced':
    if num_classes==3:
        size_increased = [1000,1000,0]
    elif num_classes==4:
        size_increased = [1000,1000,0,1500]
    elif num_classes==5:
        size_increased = [1000,120,800,1000,0]
    elif num_classes==6:
        size_increased = [1000,120,800,1000, 0, 1500]   
elif category =='min_categ':
    size_increased = [0 for i in range(num_classes)]
else:
    if num_classes==3:
        size_increased = [1000,1000,0]
    elif num_classes==4:
        size_increased = [1000,1000,0,1500]
    elif num_classes==5:
        size_increased = [1000,200,800,1000,0]   
    elif num_classes==6:
        size_increased = [1000,200,800,1000,0,1000]
        
if len(test_size)!=len(size_increased):
    sys.exit('Error in size-increased.')


return_list = get_data(dataset_type, batch, 
                       laps, 
                       space, 
                       interp_timesteps,
                       step_laps,
                       z_depth,
                       threshold_in_laps,
                       threshold_out_laps)

if len(return_list)==6:
    cal_data = return_list[0]
    cal_vel_data = return_list[1]
    cal_vel_pos_data = return_list[2]
    cal_vel_depth_data = return_list[3]

    if features==1:
        data_all = cal_data
    elif features==2:
        data_all = cal_vel_data
    elif features==3:
        data_all = cal_vel_depth_data
        
    labels_all = return_list[4]
    ids_of_cells_all = return_list[5]
    
else:
    cal_data = return_list[0]
    cal_vel_data = return_list[1]
    cal_vel_depth_data = return_list[2]
    if features==1:
        data_all = cal_data
    elif features==2:
        data_all = cal_vel_data
    elif features==3:
        data_all = cal_vel_depth_data  
    labels_all = return_list[3]
    ids_of_cells_all = return_list[4]

data, labels,ids_of_cells = take_labels(data_all, labels_all,ids_of_cells_all,indexing)

my_dict = {}
my_dict['data'] = data
my_dict['labels'] = labels
my_dict['ids_of_cells'] = ids_of_cells

fname = '../data_greg/'
fname+='data_'+str(num_classes)+'_class_'
fname+='with_features_'+str(features)+'.pkl'

with open(fname, 'wb') as file:
    pickle.dump(my_dict, file, protocol=pickle.HIGHEST_PROTOCOL)

