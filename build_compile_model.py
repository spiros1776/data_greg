# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 18:47:18 2020

@author: troullinou
"""

def build_compile_model(num_classes,input_shape):

    from keras.models import Sequential
    from keras.layers import Dense, Dropout
    from keras.layers import Conv2D, MaxPooling2D, Flatten
    from keras import regularizers
    from keras import optimizers
    from keras.layers.normalization import BatchNormalization
    from keras.utils.generic_utils import get_custom_objects    
    
    # DEFINE CONSTRUCTOR
    model = Sequential()
    
    # BUILD THE MODEL
    model.add(Conv2D(filters=256, kernel_size=(3,3), activation='relu', padding='valid', input_shape=input_shape))
    model.add(BatchNormalization())

    model.add(Conv2D(filters=128, kernel_size=(1,3), activation='relu',padding='same'))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(1,3), strides=(1,2),padding='same'))
    
    model.add(Conv2D(filters=64, kernel_size=(1,3), activation='relu',padding='same'))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(1,3), strides=(1,2),padding='same'))
    
    model.add(Conv2D(filters=32, kernel_size=(1,3), activation='relu',padding='same'))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(1,3),strides=(1,2),padding='same'))
      
    model.add(Flatten())
    model.add(Dropout(0.5)) 

    model.add(Dense(num_classes, activation='softmax'))
    
    # COMPILE THE MODEL
    adam = optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, amsgrad=False)
    model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])
        
    return model