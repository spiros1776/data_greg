#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 13:12:42 2020

@author: spiros
"""


def custom_split(data,labels,test_size,ids_of_cells,indexing,test_set='laps',seed=1000):
    import sys
    import numpy as np
    from sklearn.utils import shuffle
    from sklearn.utils import class_weight

    if data.shape[0]!=labels.shape[0]:
        sys.exit('Sizes do not match.')
        
    np.random.seed(seed)
    # SPLIT RANDOMLY TRAINING & TESTING SETS 
    # train_data, test_data, train_labels, test_labels = train_test_split(data, labels, train_size=0.8, test_size=0.2, random_state = seed, stratify=labels)
    num_classes = int(len(np.unique(labels)))
    test_data = []
    test_labels = []
    rest_data = []
    rest_labels = []
    
    test_idxs_all = []
    rest_idxs_all = []
    
    real_index = []
    for i in indexing:
        real_index+=i
    
    real_test = []
    for i in test_size:
        real_test+=i
     
    flag1 = True
    test_cells_ids = []
    for i in range(num_classes):
        nclass = real_index[i]
        label_idxs = np.where(labels==nclass)[0]
        
        if test_set=='cells':
            cells_id  = np.random.choice(np.unique(ids_of_cells[label_idxs]), real_test[i], replace=False)
            flag = True
            for jk in cells_id:
                test_cells_ids+= [jk]*len(np.where(ids_of_cells==jk)[0])
                if flag:
                    test_idxs = np.where(ids_of_cells==jk)[0]
                    flag=False
                else:
                    test_idxs = np.concatenate((test_idxs,np.where(ids_of_cells==jk)[0]))
        elif test_set=='laps':
            test_idxs = np.random.choice(label_idxs, real_test[i], replace=False)
        
        rest_idxs = np.array([jj for jj in list(label_idxs) if jj not in list(test_idxs)])
        test_idxs_all+=list(test_idxs)
        rest_idxs_all+=list(rest_idxs)
        
        if flag1:
            rest_data = data[rest_idxs]
            rest_labels = labels[rest_idxs]
            test_data = data[test_idxs]
            test_labels = labels[test_idxs]
            flag1=False
        else:
            rest_data = np.concatenate((rest_data,data[rest_idxs]), axis=0)
            rest_labels = np.concatenate((rest_labels,labels[rest_idxs]))                
            test_data = np.concatenate((test_data,data[test_idxs]), axis=0)
            test_labels = np.concatenate((test_labels,labels[test_idxs]))
    
    #check for test and train splitting        
    a = 0
    for rest_id in rest_idxs_all:
        if rest_id in test_idxs_all:
            a+=1
    
    if a!=0:
        sys.exit('Error. Mixed training-testing data.')
    
    # test_data, test_labels, test_cells_ids = shuffle(test_data, test_labels, test_cells_ids, random_state=seed)
    
    
    train_data = rest_data
    train_labels = rest_labels
    test_cells_ids = np.array(test_cells_ids)
    class_weights = class_weight.compute_class_weight('balanced', np.unique(train_labels), train_labels)

    train_data, train_labels = shuffle(train_data, train_labels, random_state=seed)
    
    return (train_data, train_labels, test_data, test_labels,test_cells_ids, class_weights)