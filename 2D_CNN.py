# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 17:47:34 2020

@author: troullinou
"""

import numpy as np
import pickle, time,os,sys

from keras.utils import to_categorical
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.utils import class_weight
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from keras.models import load_model

# IMPORT MY SCRIPTS/FUNCTIONS
from build_compile_model import build_compile_model
from custom_split import custom_split
from take_labels import take_labels, fix_training_set

epochs = 500
''' option for features: 
1 (calcium)
2(calcium-velocity)
3(calcium-velocity-position)'''
features = 3 

# Which labels we use
num_classes = 6 # Which labels we use

# class names for saving
indexing = [['BC'],['AAC'],['BISTR'],['SOM'], ['CCK'], ['NPY']]
# class names for saving
class_names = ['BC','AAC','BISTR','SOM','CCK','NPY']

if len(indexing)!=len(class_names):
    sys.exit('Error in indexing or name formats.')

# balanced test set - 75 cells / class
test_size =[[75],[75],[75],[75],[75],[75]] 

if len(test_size)!=len(class_names):
    sys.exit('Error in test-size.')
#------------------------------------------------------------------------------------------------ 

# Sizes of training set - semi-balanced
size_increased = [1000,120,800,1000, 0, 1500]   

        
if len(test_size)!=len(size_increased):
    sys.exit('Error in size-increased.')

fname = '../data_greg/'
fname+='data_'+str(num_classes)+'_class_'
fname+='with_features_'+str(features)+'.pkl'

with open(fname, 'rb') as file:
    DATA = pickle.load(file)

data = DATA['data']
labels = DATA['labels']
ids_of_cells = DATA['ids_of_cells']
     
input_shape = tuple(np.expand_dims(data[0], axis=-1).shape)

#------------------------------------------------------------------------------------------------ 

train_accuracies_all = {}
test_accuracies_all = {}

seed = 10001
np.random.seed(seed)

# CALL FUNCTION TO BUILD & COMPILE THE MODEL    
model = build_compile_model(num_classes,input_shape)

train_data_pre, train_labels_pre, test_data, test_labels,test_cells_ids, class_weights = custom_split(data, labels, test_size,ids_of_cells, indexing)   
train_data, train_labels, test_labels = fix_training_set(train_data_pre,train_labels_pre,indexing,test_labels,size_increased)

norm_train_data = train_data
norm_test_data = test_data


# PROCESS DATA & LABELS TO TRAIN AND TEST THE MODEL

# Data need reshaping as the model gets input = [samples, timesteps, features]
train_data_seq = np.expand_dims(norm_train_data,axis=-1)
test_data_seq = np.expand_dims(norm_test_data,axis=-1)
    
train_labels_seq = train_labels.reshape((len(norm_train_data),1))
one_hot_lab_train = to_categorical(train_labels,num_classes)

test_labels_seq = test_labels.reshape((len(norm_test_data),1))
one_hot_lab_test = to_categorical(test_labels,num_classes)

# SAVE MODEL
model_filename = 'best_model_split.hdf5'

es = EarlyStopping(monitor='val_accuracy', min_delta=0.005, mode='max', verbose=1, patience=200)
mc = ModelCheckpoint(model_filename, monitor='val_accuracy', mode='max', verbose=1, save_best_only=True)# TRAIN THE MODEL
tic = time.process_time() 

class_weights = class_weight.compute_class_weight('balanced', np.unique(train_labels), train_labels)
history = model.fit(train_data_seq, one_hot_lab_train, batch_size=100, epochs=epochs, validation_split=0.1,class_weight=class_weights,callbacks=[es,mc])


toc = time.process_time() 

# LOAD THE BEST MODEL SAVED
saved_model = load_model(model_filename)

# EVALUATE THE MODEL
score = saved_model.evaluate(test_data_seq, one_hot_lab_test, batch_size=16)  # get loss & accuracy
train_predictions = saved_model.predict_classes(train_data_seq)
test_predictions = saved_model.predict_classes(test_data_seq)
 
#------------------------------------------------------------------------------------------------     


# EVALUATE EACH CLASS IN TRAINING & TESTING SETS SEPERATELY

train_conf_matr = confusion_matrix(train_labels, train_predictions, normalize='true')
test_conf_matr = confusion_matrix(test_labels, test_predictions, normalize='true')

for nclass in range(num_classes):
    
    if class_names[nclass] not in train_accuracies_all.keys():
        train_accuracies_all[class_names[nclass]] = [train_conf_matr[nclass,nclass]]
        test_accuracies_all[class_names[nclass]] = [test_conf_matr[nclass,nclass]]
    else:
        train_accuracies_all[class_names[nclass]].append(train_conf_matr[nclass,nclass])
        test_accuracies_all[class_names[nclass]].append(test_conf_matr[nclass,nclass])


#------------------------------------------------------------------------------------------------ 
   

# TRAINING/TESTING
print (confusion_matrix(train_labels, train_predictions, normalize='true'))
print (confusion_matrix(test_labels, test_predictions, normalize='true'))
