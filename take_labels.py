#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 22:08:35 2020

@author: spiros
"""

def take_labels(data_all,labels_all,ids_of_cells_all,indexing):
    import numpy as np
    num_classes = len(indexing)
    data = []
    for index in indexing:
        if len(index)!=1:
            for idx in index:
                indices = np.where(labels_all==idx)[0]
                
                if len(data)==0:
                    data = data_all[indices]
                    labels= labels_all[indices]
                    ids_of_cells = ids_of_cells_all[indices]
                else:
                    data = np.concatenate((data, data_all[indices]),axis=0)
                    labels = np.concatenate((labels,labels_all[indices]))
                    ids_of_cells = np.concatenate((ids_of_cells, ids_of_cells_all[indices]))
        else:
            idx = index[0]
            indices = np.where(labels_all==idx)[0]
            if len(data)==0:
                data = data_all[indices]
                labels = labels_all[indices]
                ids_of_cells = ids_of_cells_all[indices]

            else:
                data = np.concatenate((data, data_all[indices]),axis=0)
                labels = np.concatenate((labels, labels_all[indices]))
                ids_of_cells = np.concatenate((ids_of_cells, ids_of_cells_all[indices]))

    return (data, labels,ids_of_cells)

def fix_training_set(train_data_pre,train_labels_pre,indexing,test_labels,size_increased,category='semi_balanced',balance='balanced',seed=1000):
    from take_labels import categories_balance
    import sys
    import numpy as np
    from sklearn.model_selection import train_test_split
    from sklearn.utils import shuffle
    
    sizes = categories_balance(train_data_pre,train_labels_pre,indexing,category,size_increased)
    if category!='imbalanced':
        train_data = []
        for j in range(len(indexing)):
            index = indexing[j]
            if len(index)!=1:
                train_idxs = []
                for idx in index:
                    if len(train_idxs)==0:
                        train_idxs = np.where(train_labels_pre==idx)[0]
                    else:
                        train_idxs = np.concatenate((train_idxs, np.where(train_labels_pre==idx)[0]))
                
                train_data_post = train_data_pre[train_idxs]
                train_labels_post = train_labels_pre[train_idxs]
                if balance=='stratified':
                    train_data_post2, test_data_post2, train_labels_post2, test_labels_post = train_test_split(train_data_post, train_labels_post, train_size=sizes[j], random_state = seed, stratify=train_labels_post)
                elif balance=='balanced':
                    size_i = int(sizes[j]/len(index))
                    train_data_post2   = []
                    train_labels_post2 = []
                    for i in range(len(index)):
                        if len(np.where(train_labels_post==index[i])[0])<size_i:
                            sys.exit('Error. '+idx+' has not enough training examples.')
                        train_post_idx_i = np.random.choice(np.where(train_labels_post==index[i])[0], size_i, replace=False)
                        if len(train_data_post2)==0:
                            train_data_post2 = train_data_post[train_post_idx_i]
                            train_labels_post2 = train_labels_post[train_post_idx_i]
                        else:
                            train_data_post2 = np.concatenate((train_data_post2,train_data_post[train_post_idx_i]),axis=0)
                            train_labels_post2 = np.concatenate((train_labels_post2, train_labels_post[train_post_idx_i]),axis=0)
                else:
                    sys.exit('NameError. Not a valid balance argument.')
                if len(train_data)==0:
                    train_data = train_data_post2.copy()
                    train_labels = train_labels_post2.copy()
                else:
                    train_data = np.concatenate((train_data,train_data_post2), axis=0)
                    train_labels = np.concatenate((train_labels,train_labels_post2))
            else:
                idx = index[0]
                train_idxs_all = np.where(train_labels_pre==idx)[0]
                if len(train_idxs_all)<sizes[j]:
                    sys.exit('Error. '+idx+' has not enough training examples.')
                train_idxs = np.random.choice(train_idxs_all, sizes[j], replace=False)
                train_data_post3 = train_data_pre[train_idxs]
                train_labels_post3 = train_labels_pre[train_idxs]
                if len(train_data)==0:
                    train_data = train_data_post3.copy()
                    train_labels = train_labels_post3.copy()
                else:
                    train_data = np.concatenate((train_data,train_data_post3), axis=0)
                    train_labels = np.concatenate((train_labels,train_labels_post3))
    else:
        train_data = train_data_pre
        train_labels = train_labels_pre
    
    for i in range(len(indexing)):
        index = indexing[i]
        if len(index)!=1:
            for idx in index:
                train_labels[np.where(train_labels==idx)[0]]=i
                test_labels[np.where(test_labels==idx)[0]]=i
        else:
            idx=index[0]
            train_labels[np.where(train_labels==idx)[0]]=i
            test_labels[np.where(test_labels==idx)[0]]=i
    
    train_labels = train_labels.astype(float)
    test_labels = test_labels.astype(float)
    train_data, train_labels = shuffle(train_data, train_labels, random_state=seed)
        
    return (train_data, train_labels, test_labels)


def categories_balance(train_data_pre,train_labels_pre,indexing,category,size_increased):
    import numpy as np
    import sys
    num_classes = len(indexing)
    numbers = []
    for index in indexing:
        if len(index)!=1:
            sub = []
            for idx in index:
                sub.append(train_data_pre[train_labels_pre==idx].shape[0])
            numbers.append(sub)
        else:
            idx=index[0]
            numbers.append([train_data_pre[train_labels_pre==index[0]].shape[0]])
    
    total = [sum(i) for i in numbers]    
    
    if category!='imbalanced':
        
        if category=='min_categ':
            sizes = len(total)*[np.min(total)]
        elif category=='semi_balanced':
            sizes = []
            for i in range(num_classes):
                minimum = int(np.min(total))
                sizes.append(minimum + size_increased[i])
    elif category=='imbalanced':
        sizes = numbers
    else:
        sys.exit('Not valid category!')
        
        
    return (sizes)